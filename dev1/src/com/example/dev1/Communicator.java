package com.example.dev1;

public interface Communicator {
	public void respond(String data);
}
