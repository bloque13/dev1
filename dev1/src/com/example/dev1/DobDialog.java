package com.example.dev1;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

public class DobDialog extends DialogFragment   {

	    private Button btnDone;
	    private DatePicker mDatePicker;
	    private Communicator comm;
	    
	    public DobDialog() {
	        // Empty constructor required for DialogFragment
	    }

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container,
	            Bundle savedInstanceState) {

	    	comm = (Communicator) getActivity();
	    	
	    	View view = inflater.inflate(R.layout.dob, container);
	    	
	    	mDatePicker = (DatePicker) view.findViewById(R.id.datePicker);
	    	
	    	Calendar c = Calendar.getInstance();
	        int mYear = c.get(Calendar.YEAR) - 18;
	        c.set(mYear, c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

	    	mDatePicker.setMaxDate(c.getTimeInMillis());
	    	
	        btnDone = (Button) view.findViewById(R.id.btnDone);
	        
	        btnDone.setOnClickListener(new View.OnClickListener() 
	        {

				@Override
				public void onClick(View v) {

					String day = String.valueOf( mDatePicker.getDayOfMonth());
					
					if(day.length()==1){
						day = "0"+day;
					}
					
				    String month = String.valueOf( mDatePicker.getMonth());
				    
					if(month.length()==1){
						month = "0"+month;
					}
					
				    String year =  String.valueOf( mDatePicker.getYear());

				    comm.respond(day + "/" + month + "/" + year);

					getDialog().dismiss();
	
				}
	            
	        });
	        
	
	        getDialog().setTitle("Select Date");

	        return view;
	    }



	}