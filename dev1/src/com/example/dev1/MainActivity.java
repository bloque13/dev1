package com.example.dev1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Path;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.image.SmartImageView;

// validate NI & PPS number
// modal popup for DOB with max date

public class MainActivity extends FragmentActivity implements OnClickListener, Communicator {

	private TextView textView1;
	private SmartImageView myImage;
	
	ProgressBar pb;
	Dialog dialog;
	int downloadedSize = 0;
	int totalSize = 0;
	TextView cur_val;
	String dwnload_file_path = "http://medicinesquiz.appspot.com/cppe.tester.apk";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		myImage = (SmartImageView) findViewById(R.id.my_image);
		
		myImage.setImageUrl("http://www.w3.org/html/logo/downloads/HTML5_Logo_512.png");
		textView1 = (TextView) findViewById(R.id.textView1);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	private void showEditDialog() {
        FragmentManager fm = getSupportFragmentManager();
        DobDialog dobDialog = new DobDialog();
        dobDialog.show(fm, "dob.xml");
    }

    public void onFinishEditDialog(String inputText) {
    	
        Toast.makeText(this, "Hi, " + inputText, Toast.LENGTH_SHORT).show();
    }

	@Override
	public void onClick(View v) {
		 // showEditDialog();
		
		// download apk
		
		if(v.getId() == R.id.btnClick) {
		
        showProgress(dwnload_file_path);

        new Thread(new Runnable() {
            public void run() {
                 downloadFile();
            }
          }).start();
        
		} else{
			
			installAPK();
		}
		
	}

	@Override
	public void respond(String data) {
		Log.d("app", data);
		textView1.setText(data);
	}
	
	void downloadFile(){

	    try {
	        URL url = new URL(dwnload_file_path);
	        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
	        urlConnection.setRequestMethod("GET");
	        urlConnection.setDoOutput(true);

	        //connect
	        urlConnection.connect();

	        //set the path where we want to save the file           
	        File SDCardRoot = Environment.getExternalStorageDirectory(); 
	        //create a new file, to save the downloaded file 
	        File file = new File(SDCardRoot,"cppe.tester.apk");

	        FileOutputStream fileOutput = new FileOutputStream(file);

	        //Stream used for reading the data from the internet
	        InputStream inputStream = urlConnection.getInputStream();

	        //this is the total size of the file which we are downloading
	        totalSize = urlConnection.getContentLength();

	        runOnUiThread(new Runnable() {
	            public void run() {
	                pb.setMax(totalSize);
	            }               
	        });

	        //create a buffer...
	        byte[] buffer = new byte[1024];
	        int bufferLength = 0;

	        while ( (bufferLength = inputStream.read(buffer)) > 0 ) {
	            fileOutput.write(buffer, 0, bufferLength);
	            downloadedSize += bufferLength;
	            // update the progressbar //
	            runOnUiThread(new Runnable() {
	                public void run() {
	                    pb.setProgress(downloadedSize);
	                    float per = ((float)downloadedSize/totalSize) * 100;
	                    cur_val.setText("Downloaded " + downloadedSize + "KB / " +   totalSize + "KB (" + (int)per + "%)" );
	                }
	            });
	        }
	        //close the output stream when complete //
	        fileOutput.close();
	        runOnUiThread(new Runnable() {
	            public void run() {
	                dialog.dismiss(); // if you want close it..
	                
	                installAPK();
	            }
	        });         

	    } catch (final MalformedURLException e) {
	        showError("Error : MalformedURLException " + e);        
	        e.printStackTrace();
	    } catch (final IOException e) {
	        showError("Error : IOException " + e);          
	        e.printStackTrace();
	    }
	    catch (final Exception e) {
	        showError("Error : Please check your internet connection " + e);
	    }       
	}

	protected void installAPK() {
		
		String fileName = "cppe.tester.apk";
		
        //set the path where we want to save the file           
        File SDCardRoot = Environment.getExternalStorageDirectory(); 

		// Now start the standard instalation window
		File fileLocation = new File(SDCardRoot, fileName);
		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(fileLocation),
		                       "application/vnd.android.package-archive");
		startActivity(intent);
		
	}

	void showError(final String err){
	    runOnUiThread(new Runnable() {
	        public void run() {
	            Toast.makeText(MainActivity.this, err, Toast.LENGTH_LONG).show();
	        }
	    });
	}

	void showProgress(String file_path){
	    dialog = new Dialog(MainActivity.this);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.myprogressdialog);
	    dialog.setTitle("Download Progress");

	    TextView text = (TextView) dialog.findViewById(R.id.tv1);
	    text.setText("Downloading file from ... " + file_path);
	    cur_val = (TextView) dialog.findViewById(R.id.cur_pg_tv);
	    cur_val.setText("Starting download...");
	    dialog.show();

	    pb = (ProgressBar)dialog.findViewById(R.id.progress_bar);
	    pb.setProgress(0);
	//    pb.setProgressDrawable(getResources().getDrawable(R.drawable.green_progress));  
	}

}
